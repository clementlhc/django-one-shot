from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    list = TodoList.objects.all()
    context = {
    "todo_list_list": list,
    }
    return render(request, "todos/todo_list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    # model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # todo_list = form.save(False)
            # todo_list.author == request.user
            # todo_list.save()
            form = form.save()
            return redirect("todo_list_detail", id=form.id)

    else:
        form = TodoListForm()
    context= {
        "form" : form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance=todo_list)
    context ={
        "todo_list_object": todo_list,
        "edit_form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context= {
        "form" : form,
    }
    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)

    else:
        form = TodoItemForm(instance=todo_item)
    context ={
        "edit_item_form": form,
    }
    return render(request, "todos/update_item.html", context)
